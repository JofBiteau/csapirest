﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSApiRest.Models;
using CSApiRest.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CSApiRest.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Username, model.Password);
            Console.WriteLine(value: model);
            if (user == null)
            {
                return BadRequest(new { message = "Le username ou le password est incorrect" });
            }

            return Ok(user);
        }
    }
}