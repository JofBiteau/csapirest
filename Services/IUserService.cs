﻿using CSApiRest.Models;

namespace CSApiRest.Services
{
    public interface IUserService
    {
        AuthenticateToken Authenticate(string username, string password);
    }
}